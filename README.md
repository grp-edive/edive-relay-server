# eDIVE - Relay Server

## Description
As a part of eDIVE software platform, the Relay Server is designed as centralized service, enabling interconnectin between invidual clients and servers.See the web of [eDIVE platform](https://edive.muni.cz/) for more details.

## Installation & Usage
To be detailed  later.

* Use `npm start` to run
* When building with `tsc` make sure to copy the `.env` file to the build folder (ENV variables). You can then run the build normally with `node index.js`.
* If hosting on a server in a subdirectory, add `SUBDIRECTORY` to the EVN variables (for example with the value `/servers`).


## Authors and acknowledgment
SW Design and Implementation:
* Vojtěch Brůža (FI MUNI)
* Jonáš Rosecký (FI MUNI)

The development of this SW was supported by project [EduInCIVE](https://edive.muni.cz/) - a research project of [Masaryk University](https://www.muni.cz/), funded by the [Techology Agency of the Czech Republic](https://www.tacr.cz/en//).


## License
This software is licensed under the [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.en.html). See Licence.md for full text of licence.

