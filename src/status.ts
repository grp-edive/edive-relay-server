import express from 'express';

export enum status {
    OK = 0,

    MISSING_ARGUMENT = 9,
    CODE_TAKEN = 10,
    ORG_NOT_FOUND = 11,
    INVALID_SECRET = 12,
    SERVER_NOT_FOUND = 13,
}

const messages: { [key in status]: string } = {
    [status.OK]: 'ok',

    [status.MISSING_ARGUMENT]: 'required argument is missing',
    [status.CODE_TAKEN]: 'selected code is taken',
    [status.ORG_NOT_FOUND]: 'organization not found',
    [status.INVALID_SECRET]: 'invalid server secret',
    [status.SERVER_NOT_FOUND]: 'server not found',
};

export function sendResponse(res: express.Response, stat: status, data: any) {
    if (stat === status.OK) {
        res.status(200);
    } else {
        res.status(400);
    }

    const response: {
        status: number;
        message: string;
        data: any;
    } = {
        status: stat,
        message: messages[stat],
        data: data,
    };
    res.send(response);
}
