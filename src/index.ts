import dotenv from 'dotenv';
import express, { Express, Request, Response } from 'express';
import { Manager } from './Manager';
import { Organization } from './Organization';
import { sendResponse, status } from './status';
import { nullOrUndefined } from './utils/nullOrUndefined';

dotenv.config();

const app: Express = express();
app.use(express.json());
const port = process.env.PORT;
const subdirectory = process.env.SUBDIRECTORY;

const manager = new Manager();

app.get(subdirectory + '/', (req: Request, res: Response) => {
    res.send(
        [
            '<style> table { border-collapse: collapse; } td, th { text-align: left; border: solid 1px black; padding: 5px 15px; } </style>',
            '<h1>Server is up</h1>',
            Object.values(manager.organizations)
                .map(
                    (org) =>
                        `<h3>${org.title}</h3><table><tr><th>Title</th><th>Address</th><th>Port</th><th>Code</th><th>Version</th><th>Flavour</th><th>Since last heartbeat (s)</th></tr>` +
                        org.servers
                            .map(
                                (srv) =>
                                    `<tr><td><strong>${srv.title}</strong></td><td>${srv.address}</td><td>${
                                        srv.port
                                    }</td><td>${srv.code}</td><td>${srv.version}</td><td>${srv.flavour}</td><td>${
                                        (Date.now() - srv.lastHeartbeat) / 1000
                                    }</td></tr>`,
                            )
                            .join('') +
                        '</table>',
                )
                .join('<br/>'),
        ].join(' '),
    );
});

// "server/register": {
//     "request": {
//         "address": "string",
//         "port": "number",
//         "version": "string",
//         "code": "string | null",
//         "org": "string | null"
//     },
//     "response": {
//         "status": "number",
//         "message": "string",
//         "data": {
//             "secret": "string",
//             "code": "string"
//         }
//     }
// }
app.post(subdirectory + '/server/register', (req: Request, res: Response) => {
    let { address, port, version, flavour, code, org, time } =
        (req.body as {
            address: string | null | undefined;
            port: number | null | undefined;
            version: string | null | undefined;
            flavour: string | null | undefined;
            code: string | null | undefined;
            org: string | null | undefined;
            time: number | null | undefined;
        }) || {};
    if (
        nullOrUndefined(address) ||
        nullOrUndefined(port) ||
        nullOrUndefined(version) ||
        nullOrUndefined(flavour) ||
        nullOrUndefined(time)
    ) {
        sendResponse(res, status.MISSING_ARGUMENT, {});
        return;
    }

    let organization: Organization | null;
    if (!org) {
        organization = manager.defaultOrganization;
    } else {
        organization = manager.organizationByServerCode(org);
        if (!organization) {
            sendResponse(res, status.ORG_NOT_FOUND, {});
            return;
        }
    }

    const server = organization.createServer(address!, port!, version!, flavour!, code || null, time!);
    if (!server) {
        sendResponse(res, status.CODE_TAKEN, {});
        return;
    }
    sendResponse(res, status.OK, {
        secret: server.secret,
        code: server.code,
    });
});

// "server/refresh": {
//     "request": {
//         "secret": "string",
//         "title": "string",
//         "uptime": "number",
//         "extra": "any"
//     },
//     "response": {
//         "status": "number",
//         "message": "string"
//     }
// }
app.post(subdirectory + '/server/refresh', (req: Request, res: Response) => {
    let { secret, title, extra } =
        (req.body as {
            secret: string | null | undefined;
            title: string | null | undefined;
            extra: Object | null | undefined;
        }) || {};
    if (nullOrUndefined(secret) || nullOrUndefined(title)) {
        sendResponse(res, status.MISSING_ARGUMENT, {});
        return;
    }
    const server = manager.serverBySecret(secret!);

    if (!server) {
        sendResponse(res, status.INVALID_SECRET, {});
        return;
    }

    server.refresh(title!, extra || {});
    sendResponse(res, status.OK, {});
});

// "server/dispose": {
//     "request": {
//         "secret": "string"
//     },
//     "response": {
//         "status": "number",
//         "message": "string"
//     }
// }
app.post(subdirectory + '/server/dispose', (req: Request, res: Response) => {
    let { secret } =
        (req.body as {
            secret: string | null | undefined;
        }) || {};
    if (nullOrUndefined(secret)) {
        sendResponse(res, status.MISSING_ARGUMENT, {});
        return;
    }
    const result = manager.disposeServer(secret!);

    if (!result) {
        sendResponse(res, status.INVALID_SECRET, {});
        return;
    }

    sendResponse(res, status.OK, {});
});

// "query/server": {
//     "request": {
//         "code": "string",
//         "org": "string | null"
//     },
//     "response": {
//         "status": "number",
//         "message": "string",
//         "data": {
//             "address": "string",
//             "port": "number",
//             "version": "string",
//             "title": "string",
//             "uptime": "number",
//             "extra": "any"
//         }
//     }
// }
app.get(subdirectory + '/query/server', (req: Request, res: Response) => {
    let { code, org } =
        (req.query as {
            code: string | null | undefined;
            org: string | null | undefined;
        }) || {};
    if (nullOrUndefined(code)) {
        sendResponse(res, status.MISSING_ARGUMENT, {});
        return;
    }

    let organization: Organization | null;
    if (!org) {
        organization = manager.defaultOrganization;
    } else {
        organization = manager.organizationByServerCode(org);
        if (!organization) {
            sendResponse(res, status.ORG_NOT_FOUND, {});
            return;
        }
    }

    const server = organization.serverByCode(code!);
    if (!server) {
        sendResponse(res, status.SERVER_NOT_FOUND, {});
        return;
    }

    sendResponse(res, status.OK, {
        address: server.address,
        port: server.port,
        version: server.version,
        title: server.title,
        time: server.time,
        extra: server.extra,
    });
});

// "query/org": {
//     "request": {
//         "org": "string"
//     },
//     "response": {
//         "status": "number",
//         "message": "string",
//         "data": {
//             "title": "string",
//             "servers": "number",
//             "contact": "string"
//         }
//     }
// }
app.get(subdirectory + '/query/org', (req: Request, res: Response) => {
    let { org } =
        (req.query as {
            org: string | null | undefined;
        }) || {};
    if (nullOrUndefined(org)) {
        sendResponse(res, status.MISSING_ARGUMENT, {});
        return;
    }

    let organization = manager.organizationByClientCode(org!);
    if (!organization) {
        sendResponse(res, status.ORG_NOT_FOUND, {});
        return;
    }

    sendResponse(res, status.OK, {
        title: organization.title,
        servers: organization.servers.length,
        contact: organization.contact,
    });
});

app.listen(port, () => {
    console.log(`[server]: Server is running at http://localhost:${port}`);
});
