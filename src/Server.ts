import { v4 as uuidv4 } from 'uuid';

export class Server {
    address: string;
    port: number;
    version: string;
    flavour: string;
    code: string;
    secret: string;
    time: number;
    title: string = '';
    extra: Object = {};

    lastHeartbeat: number;

    public constructor(address: string, port: number, version: string, flavour: string, code: string, time: number) {
        this.address = address;
        this.port = port;
        this.version = version;
        this.flavour = flavour;
        this.code = code;
        this.time = time;
        this.secret = uuidv4();
        this.lastHeartbeat = Date.now();
    }

    public refresh(title: string, extra: Object) {
        this.title = title;
        this.extra = extra;
        this.lastHeartbeat = Date.now();
    }

    public isAbandoned(): boolean {
        const age = Date.now() - this.lastHeartbeat;
        const ttl = process.env['SERVER_TTL'] ? parseInt(process.env['SERVER_TTL']) : 30000;
        return age > ttl;
    }
}
