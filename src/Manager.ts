import { Organization } from './Organization';
import { Server } from './Server';

export class Manager {
    public organizations: { [key: string]: Organization } = {};
    public defaultOrganization: Organization;

    public constructor() {
        this.defaultOrganization = this.createOrganization('', '', 'Public', '')!;

        setInterval(() => {
            this.garbageCollect();
        }, 30 * 1000);
    }

    public createOrganization(clientCode: string, password: string, title: string, contact: string) {
        if (Object.keys(this.organizations).includes(clientCode)) {
            return null;
        }

        const org = new Organization(clientCode, password, title, contact);
        this.organizations[clientCode] = org;
        return org;
    }

    public organizationByClientCode(code: string): Organization | null {
        return this.organizations[code] || null;
    }

    public organizationByServerCode(code: string): Organization | null {
        return Object.values(this.organizations).find((org) => org.serverCode === code) || null;
    }

    public serverByCode(org: string, code: string): Server | null {
        return this.organizations[org]?.serverByCode(code);
    }

    public serverBySecret(secret: string): Server | null {
        return (
            Object.values(this.organizations)
                .map((org) => org.serverBySecret(secret))
                .find((srv) => srv) || null
        );
    }

    public disposeServer(secret: string): boolean {
        return Object.values(this.organizations)
            .map((org) => org.disposeServer(secret))
            .some((res) => res);
    }

    public garbageCollect() {
        Object.values(this.organizations).forEach((org) => org.garbageCollect());
    }
}
