import { v4 as uuidv4 } from 'uuid';
import { Server } from './Server';

export class Organization {
    public clientCode: string;
    public serverCode: string;
    public password: string;
    public title: string;
    public contact: string;

    public servers: Server[] = [];

    public constructor(clientCode: string, password: string, title: string, contact: string) {
        this.clientCode = clientCode;
        this.serverCode = uuidv4();
        this.password = password;
        this.title = title;
        this.contact = contact;
    }

    public createServer(
        address: string,
        port: number,
        version: string,
        flavour: string,
        code: string | null,
        time: number,
    ): Server | null {
        if (code && this.servers.some((server) => server.code === code)) {
            return null;
        }
        if (!code) {
            do {
                code = Math.floor(Math.random() * 9000 + 1000).toString();
            } while (this.servers.some((server) => server.code === code));
        }

        const server = new Server(address, port, version, flavour, code, time);
        this.servers.push(server);

        return server;
    }

    public serverByCode(code: string): Server | null {
        return this.servers.find((server) => server.code === code) || null;
    }

    public serverBySecret(secret: string): Server | null {
        return this.servers.find((server) => server.secret === secret) || null;
    }

    public disposeServer(secret: string): boolean {
        if (this.servers.some((server) => server.secret === secret)) {
            this.servers = this.servers.filter((server) => server.secret !== secret);
            return true;
        }
        return false;
    }

    public garbageCollect() {
        this.servers = this.servers.filter((server) => !server.isAbandoned());
    }
}
