export function nullOrUndefined(data: any) {
    return data === null || data === undefined;
}
